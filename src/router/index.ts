import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'home',
        component: () => import('../home/HomeIndex.vue')
    },
  /**  element plus */
    {
        path: '/ElTableIndex',
        name: 'ElTableIndex',
        component: () => import('../views/elementPlus/el-table/ElTableIndex.vue')
    },
  {
        path: '/FormExample',
        name: 'FormExample',
        component: () => import('../views/elementPlus/form/FormExample.vue')
    },
    
    /**  vue3 */
    {
        path: '/GetStartedQuicklyDoc',
        name: 'GetStartedQuicklyDoc',
        component: () => import('../views/vue3/getStartedQuickly/GetStartedQuicklyDoc.vue')
    },

]

const router = createRouter({
    history: createWebHistory('/vue3Demo'),
    routes
})

export default router