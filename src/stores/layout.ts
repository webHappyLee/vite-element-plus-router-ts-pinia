import {defineStore} from 'pinia'

// import {TabItem} from '@/types/layout'

export const layout = defineStore('layout', {
  
  state: () => ({
    isCollapse: false,
    tabActiveIndex: 0,
    menuList: [
      {
        id: 1,
        label: '首页',
        icon: 'HomeFilled',
        path: '/Home',
        index: '0',
        isIframe: false,
        children: []
      },
      {
        id: 2,
        label: '系统设置',
        icon: 'Tools',
        path: '/SystemSettings',
        index: '1',
        isIframe: false,
        children: [
          {
            id: 3,
            label: '菜单管理',
            icon: 'Grid',
            path: '/MenuManagement',
            index: '1-1',
            isIframe: false,
            children: []
          },
          {
            id: 4,
            label: '用户管理',
            icon: 'Flag',
            path: '/UserManagement',
            index: '1-2',
            isIframe: false,
            children: []
          }, {
            id: 5,
            label: '部门管理',
            icon: 'PriceTag',
            path: '/Department',
            index: '1-3',
            isIframe: false,
            children: []
          }, {
            id: 6,
            label: '字典管理',
            icon: 'Stopwatch',
            path: '/DictionaryManaged',
            index: '1-4',
            isIframe: false,
            children: []
          }, {
            id: 7,
            label: '我们管理',
            icon: 'GoldMedal',
            path: '/WeManage',
            index: '1-5',
            isIframe: false,
            children: []
          },
        ]
      },
      {
        id: 8,
        label: '权限管理',
        icon: 'InfoFilled',
        path: '/RightsManagement',
        index: '2',
        isIframe: false,
        children: [
          {
            id: 81,
            label: '权限管理-1',
            icon: 'InfoFilled',
            path: '/RightsManagement-1',
            index: '21',
            isIframe: false,
            children: []
          }
        ]
      },
      {
        id: 202401,
        label: 'Element Plus',
        icon: 'ElementPlus',
        path: '/ElementPlus',
        index: '20240106',
        isIframe: false,
        children: [
          {
            id: 20240101,
            label: '表格',
            icon: 'Grid',
            path: '/ElTableIndex',
            index: '20240101',
            isIframe: false,
            children: []
          },
          {
            id: 20240110,
            label: '表单',
            icon: 'Menu',
            path: '/FormExample',
            index: '20240110',
            isIframe: false,
            children: []
          },
        ]
      },
      {
        id: 20240107,
        label: 'Vue3',
        icon: 'ElementPlus',
        path: '/Vue3',
        index: '20240107',
        isIframe: false,
        children: [
          {
            id: 2024010708,
            label: '快速上手',
            icon: 'Grid',
            path: '/GetStartedQuicklyDoc',
            index: '2024010708',
            isIframe: false,
            children: []
          }
        ]
      },
      {
        id: 9,
        label: '菜单嵌套',
        icon: 'HelpFilled',
        path: '/MenuNest',
        index: '3',
        isIframe: false,
        children: []
      },
      {
        id: 10,
        label: '功能',
        icon: 'StarFilled',
        path: '/Function',
        index: '4',
        isIframe: false,
        children: []
      },
      {
        id: 11,
        label: '页面',
        icon: 'Histogram',
        path: '/page',
        index: '5',
        isIframe: false,
        children: []
      },
      {
        id: 12,
        label: '组件封装',
        icon: 'Promotion',
        path: '/ComponentPack',
        index: '6',
        isIframe: false,
        children: []
      },
      {
        id: 13,
        label: '路由参数',
        icon: 'Management',
        path: '/RoutingParameters',
        index: '7',
        isIframe: false,
        children: []
      },
      {
        id: 14,
        label: '数据可视化',
        icon: 'Checked',
        path: '/DataVisualization',
        index: '8',
        isIframe: false,
        children: []
      },
      {
        id: 15,
        label: '大数据图表',
        icon: 'Ticket',
        path: '/BigDataCharts',
        index: '9',
        isIframe: false,
        children: []
      },
      {
        id: 16,
        label: '个人中心',
        icon: 'Avatar',
        path: '/PersonalCenter',
        index: '10',
        isIframe: false,
        children: []
      },
      {
        id: 17,
        label: '工具栏集合',
        icon: 'List',
        path: '/ToolbarCollection',
        index: '11',
        isIframe: false,
        children: []
      },
      {
        id: 18,
        label: '外链',
        icon: 'Paperclip',
        path: '/Link',
        index: '12',
        isIframe: false,
        children: []
      },
    ],
    
    breadcrumbList: [{
      id: 1,
      label: '首页',
      icon: 'HomeFilled',
      path: '',
      index: '0',
      isIframe: false,
      children: []
    }],
    tags: [
      {
        id: 1,
        label: '首页',
        icon: 'HomeFilled',
        path: '',
        index: '0',
        isIframe: false,
        children: []
      },
    ]
  }),
  
  actions: {
    switchCollapse() {
      this.isCollapse = !this.isCollapse
    },
    
    setCurrentTabIndex(n: number) {
      this.tabActiveIndex = n
    },
    
    /** 新增tab */
    setTabAdd(tab: any) {
      
      let isHad = false
      
      this.tags.forEach((item, index) => {
        if (item.id === tab.id) {
          this.tabActiveIndex = index
          isHad = true
        }
      })
      
      if (!isHad) {
        this.tabActiveIndex = this.tags.length
        this.tags.push(tab)
      }
    },
    
    setTabClose(index: number) {
      if (this.tags.length === 1 || index === 0) return;
      
      if (this.tabActiveIndex === 0) {
        this.tags.splice(index, 1)
        this.tabActiveIndex = 0
        return
      }
      
      if (this.tabActiveIndex === index) {
        this.tags.splice(index, 1)
        this.tabActiveIndex = index - 1
        return;
      }
      
      if (this.tabActiveIndex < index) {
        this.tags.splice(index, 1)
        return;
      }
      
      if (this.tabActiveIndex > index) {
        this.tags.splice(index, 1)
        this.tabActiveIndex--
        return;
      }
    },
    
    /** 面包屑 */
    setBreadcrumb(breadcrumb: any) {
      
      let param = {
        id: 1,
        label: '首页',
        icon: 'HomeFilled',
        path: '',
        index: '0',
        isIframe: false,
        children: []
      }
      
      this.breadcrumbList = breadcrumb
      
      this.breadcrumbList.unshift(param)
      
    }
  },
})