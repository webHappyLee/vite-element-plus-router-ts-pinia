export const getPathByKey = (currentKey, data, keyName = 'id') => {
  let result = []
  let traverse = (currentKey, path, data) => {
    if (!data || !data.length) return
    
    for (let item of data) {
      path.push(item)
      
      if (item[keyName] === currentKey) {
        result = JSON.parse(JSON.stringify(path))
        return;
      }
      
      const children = Array.isArray(item.children) ? item.children : []
      traverse(currentKey, path, children)
      
      path.pop()
      
    }
  }
  
  traverse(currentKey, [], data)
  
  return result
}
