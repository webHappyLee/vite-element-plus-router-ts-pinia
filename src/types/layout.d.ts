export interface TabItem {
  id: number | string,
  label: string,
  icon: string,
  path: string,
  index: string | number,
  isIframe: boolean,
  children: Array<TabItem>
}